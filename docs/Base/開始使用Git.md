開始使用 Git 
====================  

## Git 安裝
-------

常用的GIT軟體:

* Git for Windows:  
  ![](../img/GitForWin.png)
  [下載連結](https://git-scm.com/)  

!!! tip  
    若使用SmartGit 可以不需安裝，SmartGit 內建就有含Git bash command

* SmartGit**(建議使用)**:  
  ![](../img/SmartGit.png)  
  [穩定版連結](http://www.syntevo.com/smartgit/)  
  [預先發行版連結](http://www.syntevo.com/smartgit/preview)

* Tortoisegit**(建議使用)**:  

  ![](../img/Tortoisegit.png)
  [下載連結](https://tortoisegit.org/)  
  
!!! tip 
    若您只安裝SmartGit，Tortoisegit 可選擇 SmartGit git exe path :  
  
    ![](../img/Tortoisegit_1.png)

* Sourcetree (太佔資源不建議使用):  

  ![](../img/Sourcetree.png)
  [下載連結](https://www.sourcetreeapp.com/) 
  
## Smartgit 使用方式
------

### 介面介紹
------
<iframe width="854" height="480" src="https://www.youtube.com/embed/DdlDfIis-3U" frameborder="0" allowfullscreen></iframe>
