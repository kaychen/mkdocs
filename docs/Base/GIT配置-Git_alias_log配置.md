Git alias & log 配置
====================

## Git alias 配置

>我們在用git的時候，很多時候都是用的命令行形式的，不僅要記得住大量命令，還要能靈活進行組合，這就很是頭大了，正因為此，命令別名的重要性就出來了。但與其說是別名，倒不如說是另類的簡寫形式。

### example:

`給 git status 設置別名 st`

    git config --global alias.st status

###  git log 排版配置:

    git config --global alias.lg log --color --graph --pretty=format:'%C(bold red)%h%C(reset) - %C(bold green)(%cr)%C(bold blue)<%an>%C(reset) -%C(bold yellow)%d%C(reset) %s' --abbrev-commit`

!!! note 

    %h 表示提交id；
    %cr 表示提交时间；
    %an 表示提交人；
    %d 表示 分支、tag、HEAD 等信息；
    %s 表示提交的信息。

效果:  

![](../img/Git_alias.png)  
