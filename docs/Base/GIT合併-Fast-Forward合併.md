GIT合併- Fast-Forward合併
====================

## Use Git command
-------

    mkdir merge2
    cd merge2
    git init
    echo 1 > file1.txt
    echo 1 > file2.txt
    git add file1.txt
    git commit -m "C1"
    git add file2.txt
    git commit -m "C2"  

-------
    git checkout -b hotfix
    echo 1 > file3.txt
    git add .
    git commit -m "C3"
    echo 1 > file4.txt
    git add .
    git commit -m "C4"

在master分支有以下檔案：  

    file1.txt、file2.txt
在hotfix分支有以下檔案：  

    file1.txt～file4.txt
>由於master在C2分出hotfix後，在hotfix繼續提交了C3、C4版本，因此hotfix停在C4。  

而master在分出hotfix後，就沒有再作業提交，因此還停在C2。
此時，如果要將C3、C4版本做的變更合併至master裡，可下指令： 

    git checkout master
    git merge hotfix

合併完後的命令提示及分支圖(請注意圖中Fast-Forward這個字)：  
![](../img/2017-06-04-23-41-12.png)

你會發現，分支圖和之前有不同之處：    
![](../img/2017-06-04-23-42-43.png)  

> 1. 不像之前合併的樣子，有從旁分一個分支再合回來(長耳朵)  
> 2. 沒有在合併後，多一個提交(commit)  

這種情形我們稱為 ```快轉合併(Fast-Forward)```。會產生快進的情形，就是要合併進來的hotfix分支，剛好在master分支的前面，並沒有額外分叉出去，此時只要將master分支的指標往前移動，就解決合併了，因此GIT做了Fast-Forward，省得麻煩還要再建commit

可是，如果想看到有長出耳朵的那種合併，怎麼做呢？
首先，我們復原剛才的合併，怎麼做還記得吧：

    git reset --hard ORIG_HEAD
接著重做一次合併，但這次要加--no-ff指令
    
    git merge --no-ff hotfix
完成後，就長出耳朵來了：  

![](../img/2017-06-04-23-44-26.png)

## Use Smartgit
-------
![](../img/Git-Fast-Forward-merge.gif)

!!! tip ""

    smartgit 未加入 --no-ff 參數 需要自行修改

