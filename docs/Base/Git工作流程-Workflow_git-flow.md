Git Workflow (git-flow) 
====================
> 為什麼使用Git Flow?  
Git在開發上非常方便，但也因為可以很方便的把各自的負責的部份切開，一旦 commit 和維護的人一多，就很容易顯得 branch 非常的雜亂，主要版本如果過度的小部 commit ，就會顯得很沒意義，因為並沒有辦法很快速的看出版本切割在哪(雖然可以用 tag 之類的)，所以 Git Flow 的概念就是將主要的 branch 分為 master和develop，master就是最後的發佈版本，所有維護者在發佈前的都是對develop進行操作，這樣就不會讓發佈版本的 branch 看起來過度雜亂。


## 安裝Git Flow
--------------

Git Flow 並不是本來就在 Git 裡面的功能，所以需另外安裝[(GitHub連結)](https://github.com/nvie/gitflow)，有很多 Git 的 GUI 操作軟體(eg: Smartgit、Sourcetree)本身就可以操作Git Flow 就不用另外再安裝。

## Git Flow 流程圖 
--------------

![](../img/2017-06-18-22-11-43.png)


主要分支
--------------
    master: 只接受 develop 和 Release 的 merge (永遠處在 production-ready 狀態)
    develop: 所有 Feature 開發都從這分支出去，完成後merge回來 (於最新的下次發佈開發狀態 )
支援性分支  
--------------

   ```Feature branches:``` 開發新功能都從 develop 分支出來，完成後 會merge 回 **develop**  
   
   ```Release branches:``` 準備要 release 的版本，只修 bugs。從 develop 分支出來，完成後 merge 回  **master** 和 **develop**  

   ```Hotfix branches:``` 等不及 release 版本就必須馬上修 master 趕上線的情況(火燒屁股的 bug 必須馬上解決時)。會從 master 分支出來，完成後直接 merge 回 **master** 和 **develop**  

!!! note  

    除Feature branches 會merge 至 develop 外，其他branches 完成後皆會自動 merge 回 master  

## 操作Git Flow (Use Smartgit)
--------------

## Cofigure Gitflow 
-------------------

>你可以選擇只有Feature 分支 的 Light mode 或者所有分支的Full mode(建議使用 Full mode)  

![](../img/Gitflow_init.gif)  

    Configure 完成後左下角會看到 develop 分支

![](../img/2017-07-02-22-24-53.png)


## Feature Development
-------------------

### 概念圖: 

![](../img/2017-06-18-22-22-35.png)  

### 範例說明:

 > 案例: 今天在專案Source code 準備新增3個功能:要如何操作呢??

假設預計要新增功能 1，並想切換分支進行測試，如下操作。

![](../img/Gitflow_addfeature.gif)


!!! note  
  
    完成後左下角會看到 Feature 分支，Feature 命名可以是中文

新增功能，並測試OK後，我們需執行 finish feature 讓目前修改合併至develop 分支 ，我們可以這樣做。

## Release Development
-------------------

![](../img/2017-06-18-22-35-57.png)

## Hotfix Development
-------------------

![](../img/2017-06-18-22-36-16.png)

## Spport Development (option)
-------------------


> 這是gitflow一個實驗性的功能，所以在使用上必須非常謹慎，接下來我們將講解如何去使用他:  
先前提到，當你必須去支援較舊的版本時，就必須使用這個方法去建立分支，通常會發生在一些懶得升級的用戶端，但他們卻又有預算可以分配給這些開發， 
即時他們知道這可能會導致一些問題的發生。如果你要這樣相容舊版本是對的，但我不認為他是一個好主意。軟體在現今變化速度很快，我們必須阻止客戶繼續停留在舊版本上，但很無奈的是，出錢的說話最大。

![](../img/2017-06-18-22-37-00.png)




Ref:


https://blogs.endjin.com/2013/04/a-step-by-step-guide-to-using-gitflow-with-teamcity-part-3-gitflow-commands/


http://danielkummer.github.io/git-flow-cheatsheet/index.zh_TW.html

http://www.codeceo.com/article/how-to-use-git-flow.html


https://github.com/petervanderdoes/gitflow-avh

https://github.com/stevemao/awesome-git-addons#git-hooks

* [Git Workflows That Work](https://www.endpoint.com/blog/2014/05/02/git-workflows-that-work)


* [Getting Started – Git-Flow](https://yakiloo.com/getting-started-git-flow/)  

* [A successful Git branching model](http://nvie.com/posts/a-successful-git-branching-model/)  

* [Git flow 開發流程](https://ihower.tw/blog/archives/5140)  
* [Git-Flow 使用筆記](http://fireqqtw.logdown.com/posts/206951-git-flow-notes)  
