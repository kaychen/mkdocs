Git 配置說明
====================

>  [Git Config 說明](https://git-scm.com/docs/git-config)  


## 不同級別的配置

`git config 有三個層級，設定優先權: local > global > system `

* 系統 --system (需要 root 權限)

    * Windows: C:\Program Files (x86)\Git\etc\gitconfig
    * Linux: /etc/gitconfig
    * OS X: /usr/local/git/etc/gitconfig
* 使用者 --global

    * Windows: C:\Users\.gitconfig
    * Linux: ~/.gitconfig
    * OS X: ~/.gitconfig

* 儲存庫 --local (default)

    * /.git/config

## 查詢目前 Repo 目前config 設定指令:

當前設定:

    git config --list

global: 

    git config --global --list 

local:

    git config --local --list


## 參考來源: 

<http://clouding.city/git-tips/>  

<http://starzhou.com/blogs/git_configuration>  

<https://segmentfault.com/a/1190000009369889>  

<https://www.liaoxuefeng.com/wiki/0013739516305929606dd18361248578c67b8067c8c017b000>  

https://gist.github.com/danny0838/11acfa65824940d426cd3f4403ca4ed4
