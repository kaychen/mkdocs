GIT 分支 - (建立、切換、作業)
====================

> branch (分支)應該是 Git 最重要的技能了，在一個多人專案的開發過程中我們有時候要開發新功能，有時候是要修正某個Bug，有時候想要測試某個特異功能能不能 work ，這時候我們通常都會從主 branch 再開出一條新的 branch 來做，這支新開的 branch 會帶著你的主 branch 目前的最新狀態，當你完成你所要開發的新功能/ Bug 修正後確認沒問題就再把它 merge(合併)回主 Branch ，如此便完成了新功能的開發或是 Bug 的修正，因此每個人都可以從主 branch 拉一條新的 branch 來做自己想做的事，再來我們好好了解一下 branch 的使用。

## 分支建立、切換
-------
### Use Git command
-------

    mkdir branch1
    cd branch1
    git init
    echo 1 > file1.txt
    echo 1 > file2.txt
    git add file1.txt
    git commit -m "C1"
    git add file2.txt
    git commit -m "C2"

查看目前分支只要下branch即可，星號表示目前分支：

    git branch
若要在master另開分支，例如要開feature分支，做法是：

    git branch feature

建好後，要切換至新的分支上，用checkout指令就好了：
    
    git checkout feature

此時再下一次查看分支的指令，就可以看到星號指向feature了：

    git branch

演示圖:  

![](../img/2017-06-04-16-41-09.png)

其實還有更快的指令：

    git checkout -b feature
    
這個指令包含建立分支及切換分支，所以一個指令等同於兩個指令

>【git checkout -b feature】 = 【git branch feature】 + 【git checkout feature】

看一下分支圖，此時兩分支皆指向C2這個commit：  

![](../img/2017-06-04-16-47-03.png)

### Use Smartgit
-------

 ![](../img/Git-branch.gif)


## 不同分支進行作業
-------
### 範例說明
-------
接著我想在feature分支建一個file3.txt檔案，並且提交(commit)
    
    git checkout feature
    echo 1 > file3.txt
    git add .
    git commit -m "C3"

接著又在feature分支建file4.txt檔，並且提交(commit)

    echo 1 > file4.txt
    git add .
    git commit -m "C4"
此時的分支圖，想像一下長什麼樣子呢？  

![](../img/2017-06-04-21-02-39.png)  
>有沒有發現，這兩個分支已經漸漸分開了，而且master還停留在C2這個commit上。

接下來我們要換分支做事情了，先切換到master分支，接著建立file5.txt和file6.txt，並分別提交C5、C6兩個版本

    git checkout master
    echo 1 > file5.txt
    git add .
    git commit -m "C5"
    echo 1 > file6.txt
    git add .
    git commit -m "C6"

做好之後，一樣想像一下分支圖長什麼樣子？

看答案吧！哇！總算看到「分支」了：  

![](../img/2017-06-04-21-10-18.png)

再來要考考大家了，如果將分支切換到master，此時開啟資料夾，你覺得會有哪些檔案呢？

切至master分支的檔案如下：  

![](../img/2017-06-04-21-13-43.png)

切換至feature分支的檔案如下：  

![](../img/2017-06-04-21-14-13.png)

>有沒有看到GIT版控的強大啊！只要切換到該分支，就會馬上呈現出當時版本的檔案內容，而且切換的時間超短，即時你的檔案很多，切換速度也是粉…快喔！

來解釋一下，為什麼這兩個分支的檔案會變成這樣呢？做個比喻好了，你可以把每個分支想像成不同的水流：  

![](../img/2017-06-04-21-15-13.png)

>水流的上游在左邊，下游在右邊，由左往右流。
分支的提交(commit)較早在左邊，較晚在右邊，時間由左往右推進。

先看master，從下游往上回溯，此條流水經過： 

    file1.txt、file2.txt、file5.txt、file6.txt
再看feature，從下游往上回溯，此條流水經過：  

    file1.txt、file2.txt、file3.txt、file4.txt

所以此兩分支的檔案才會長成這樣子囉！ 

