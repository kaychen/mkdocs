換行問題配置
====================

 > 如果Team使用不同操作系統做開發，由於windows和linux對換行的處理不同（windows使用CRLF作為一行的結束，linux使用LF），這樣當一個人在linux機器上推代碼，而另一個人在windows機器上拉代碼後查看狀態，發現文件的狀態都是修改，使用checkout -f也無濟於事，下面的配置則可以解決這一個問題。

### 解決方式:

`windows`

如果是在Windows 系統上，把它設置成 true，這樣當 check out 程式的時候，LF 會被轉換成 CRLF：

    git config --global core.autocrlf true

`linux:`  
Linux 或 Mac 系統使用 LF 作為行結束符，因此你不希望 Git 在 check out 檔案時進行自動的轉換；但是，當一個以 CRLF 做為換行符號的檔案不小心被引入時，你肯定希望 Git 可以修正它。你可以把 core.autocrlf 設置成 input 來告訴 Git 在提交時把 CRLF 轉換成 LF，check out 時不轉換：  

    git config --global core.autocrlf input


 若沒有跨 OS 平台存取的需要，所以將 autocrlf 設為 false，也就是告訴 Git：別幫我轉換 CRLF 字元。

    git config --global core.autocrlf false

!!! note  

    input CRLF -> LF
    true LF -> CRLF
    false -> 與原檔案格式一樣，Git 操作時不進行轉換
