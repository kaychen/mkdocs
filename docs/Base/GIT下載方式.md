GIT 下載方式
====================

## Git Clone
-------

- [git_clone](Base/GIT常用指令-git_clone/)

## Git fetch  (Tag)
-------------

Step 1. 建立一個本地儲存庫
    
    git init

Step 2. 加入遠端repository
    
    git remote add origin <path> 

!!! note ""  

    可以使用git remote -v 查詢遠端repository

Step 3. 執行git fetch

    git fetch origin <tag>

Step 4. 將當前的 FETCH_HEAD 本地端加上 tag

    git tag <tag> FETCH_HEAD


Ref:  

<https://stackoverflow.com/questions/45338495/fetch-a-single-tag-from-remote-repository>