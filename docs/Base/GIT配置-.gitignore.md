定義 .gitignore  
====================

>在 Git 裡面，是透過 .gitignore 檔案來進行定義「忽略清單」，主要的目的是排除一些不需要加入版控的檔案，列在裡面的檔名或路徑 (可包含萬用字元)，都不會出現在 git status 的結果中，自然也不會在執行 git add 的時候被加入。不過，這僅限於 Untracked file 而已，那些已經列入版控的檔案 (Staged file)，不受 .gitignore 檔案控制。

## .gitignore 格式
-----

    #  <=註解
    *.png               #png 圖檔
    !new.png            #new.png 例外
    /images             #/images 目錄下檔案
    images/             #所有 images 目錄下檔案

    new?.png            #new[0-9,a-Z].png , new10.png 不會被忽略
    new[a-z].png        #new[a-z].png, new[0-9].png 不會被忽略
    new[0-9].png        #new[0-9].png, new[a-z].png 不會被忽略
    new[!01].png        #new[2-9].png, new0.png、new1.png、new01.png 不會被忽略

## 列出 gitignore 檔案清單 (可用來檢查用)
----
    git ls-files --others -i --exclude-standard

## 設定本機端公用的 gitignore
----
新增一的檔案 ~/

linux:

    cd ~/
    vim .gitignoreglobal

windows:
    
    新增一個檔名為.gitignoreglobal 至  
    C:\Users\[username]\

執行以下指令生效:    
    
    git config --global core.excludesfile ~/.gitignoreglobal
## git忽略已經被提交的文件
----
### 方法一 (只適用本地操作)
----
    git update-index --assume-unchanged <PATH>


 被採納的答案雖然能達到（暫時的）目的，但並非最正確的做法，這樣做是誤解了git update-index的含義，而且這樣做帶來的最直接（不良）後果是這樣的

所有的團隊成員都必須對目標文件執行：git update-index --assume-unchanged <PATH>。這是因為即使你讓Git假裝看不見目標文件的改變，但文件本身還是在Git的歷史記錄裡的，但所有團隊的每個人在fetch的時候都會拉到目標文件的變更（但實際上目標文件是根本不想被Git記錄的，而不是假裝看不見它發生了改變）

一旦有人改變目標文件之後沒有git update-index --assume-unchanged <PATH>就直接push了，那麼接下來所有拉取了最新代碼的成員必須重新執行update-index，否則Git又會開始記錄目標文件的變化。這一點實際上很常見的，比如說某成員換了機器或者硬盤，重新克隆了一份代碼庫，由於目標文件還在Git的歷史記錄裡，所以他/她很可能會忘記update-index。

### 方法二 (建議使用此方式)
----
    git rm --cached <PATH>

> 先了解 `git rm --cached`的背後原理 :
> 若檔案存在於stage與repository時，會將檔案從repository刪除，並且從stage刪除，但不會刪除working directory的實際檔案，不過由於檔案已經從repository刪除，檔案會從`tracked`變成`untracked`。
> 若檔案存在於stage，卻不存在於repository，會將檔案從stage刪除，但不會刪除working director的實際檔案，因為repository本來就沒有這個檔案，所以一樣是`untracked`不變。

>回想我們的狀況 :

>若該檔案不在repository內 : `git rm --cached`會幫我們從stage刪除，且檔案本來就是untracked，執行完還是untracked，符合我們的預期。

>若檔案已經在repository內 :`git rm --cached`會幫我們從repository刪除，並且從stage刪除，因為已經從repository刪除檔案，檔案會從tracked變成untracked，這並不是我們預期的。

>這解釋了為什麼當檔案不在repository時，必須下`git rm --cached`。

### 操作流程
----    

#### Use Git command
----
    1. git rm --cached file
    2. git add .gitignore  
        **(不要將removed add to stage)**
    3. git commit -m "Refresh adding .gitignore file."

#### Use SmartGit
----

1. 在欲處理的檔案點右鍵 -> Remove...

2. 會跳出以下視窗(若你要刪除此檔案請選擇**Delete local file**):
    ![](../img/Git_remove.png)
3. 此時狀態會變成`Removed`

    ![](../img/Git_remove_1.png)

4. Stage .gitignore  
    **(不要將removed 的檔案也執行Stage)**
5. Commit ...

!!! note

    另外git還提供了另一種排除的方式來做同樣的事情，不同的是.gitignore這個文件本身會提交到版本庫中去。用來保存的是公共的需要排除的文件。而.git / info / exclude這裡設置的則是你自己本地需要排除的文件。他不會影響到其他人。也不會提交到版本庫中去。

!!! note 

    .gitignore 還有個有意思的小功能， 一個空的 .gitignore 文件 可以當作是一個 placeholder 。當你需要為項目創建一個空的 log 目錄時， 這就變的很有用。你可以創建一個 log 目錄 在裡面放置一個空的 .gitignore 文件。這樣當你 clone 這個 repo 的時候 git 會自動的創建好一個空的 log 目錄了。


## Gitignore example

<https://github.com/github/gitignore>


ref:

<https://git-scm.com/docs/gitignore>
<https://www.atlassian.com/git/tutorials/gitignore>
  
<http://oomusou.io/git/git-remove-stage/>
<http://www.ifeegoo.com/git-code-management-dot-gitignore-file-has-no-effect-solution.html>

<https://segmentfault.com/q/1010000000430426>

