GIT暫存-git worktree
====================

>Git 的 [worktree](https://git-scm.com/docs/git-worktree) 是 Git 2.6 新增的功能，有點相見恨晚啊。


    git worktree add [-f] [--detach] [--checkout] [--lock] [-b <new-branch>] <path> [<branch>]

## 應用說明
有在用 Git 的都知道如果工作目錄(working tree)修改到一半的話，是不能隨便切換 `branch` 的。解決方式是要麻是 `git stash` 先暫存起來，不然就是先暫時 commit 等會再 reset 回來。這都還好。
但是，還是會碰到一些需要同時**處理不同 branch** 的情況，例如想要同時修改或對照不同 branch，或是一邊跑測試一邊繼續寫code，甚至是想同時開發測試不同 branch 等等。這時候就得另外 clone 一個目錄，但這就是麻煩的開始，不同的 repo 目錄要怎麼同步最新的修改? 重新複製目錄? 先 `git push` 再 `pull` 一次? 總之就是有點繞啊。

## 操作

git worktree 這個神奇的指令可以創造出另一個工作目錄，例如：

    git worktree add -b hotfix ../hotfix master

Command report:

    Preparing ../hotfix (identifier hotfix)
    HEAD is now at 46d363dc XXX

!!! note 

    如此就會在上一層新建立一個 hotfix 目錄，並新建一個分支 hotfix。這種工作目錄叫做 linked working tree。

開啟/切換到hotfix 目錄下 (cd ../hotfix) 就是一個乾淨從 master 分出來的 hotfix 分支，可以在這邊做繼續開發。

而產生的 hotfix 目錄有個 .git 檔案，內容只說明 worktrees 的路徑

    gitdir: C:/GitDemo/Test/.git/worktrees/hotfix

最後完成測試後執行 commit ，將會自動合併至你當時切換出來的分支。

可以直接刪除這個 hotfix 目錄(東西已經進 branch 有commit 就會有紀錄)，

接著可以用以下指令清除 linked working tree 記錄:
    
    git worktree prune 

!!! note

    不清除也沒關係，三個月後也會自動清掉。

##  worktree lock

不過如果你把 linked working tree 放在會被移除的裝置上，例如外接硬碟上，這樣可能會不小心被 Git 清掉。這時候需要用以下指令。
    
    git worktree lock
    
##  worktree list
可以使用以下指令列出所有的工作目錄:

    git worktree list


## **BUGS**

Multiple checkout in general is still experimental, and the support for **submodules** is incomplete. It is NOT recommended to make multiple checkouts of a superproject.

git-worktree could provide more automation for tasks currently performed manually, such as:

* `remove` to remove a linked working tree and its administrative files (and warn if the working tree is dirty)
       
* `mv` to move or rename a working tree and update its administrative files