為何需要使用版本控制系統?
====================
要把檔案恢復到編輯前的狀態，大家都是怎麼做的呢？
最簡單的方法就是先**複製編輯前的檔案**，使用這個方法時通常都會在檔案名稱或目錄名稱上**添加編輯的日期。**但是，每次編輯檔案都要複製非常的麻煩，也很容易出錯。  

 ![](../img/2017-05-29-22-15-07.png)  
像上面的例子那樣毫無規則的命名的話，就沒有辦法區別那一個檔案是最新的了。如果是組群共享操作的檔案要加上編輯者的名字，具體進行了什麼樣的更改也不容易知道了。
而且，組群共享的檔案，如果有兩個人同時進行編輯的話，先進行編輯的人的修改內容會被覆蓋，相信大家都有這樣的經歷。  
 
 ![](../img/2017-05-29-22-15-23.png)  
 
版本控制系統就是為了解決這樣的問題而開發的。  

目前版本控制的類型分成三類:  

## 1. 本地版本控制系統( Local Version Control Systems)
---

**在自己的電腦建立一個版本資料**

許多人採用複製檔案到其它目錄的方式來做版本控制（若他們夠聰明的話，或許會是有記錄時間戳記的目錄）。 因為它很簡單，這是個很常見的方法；但它也很容易出錯。 讀者很容易就忘記在哪個目錄，並不小心地把錯誤的檔案寫入、或者複製到不想要的檔案。

為了解決這問題，程式設計師在很久以前開發了本地端的版本控制系統，具備簡單的資料庫，用來記載檔案的所有變更記錄。

 ![](../img/2017-05-29-22-23-14.png) 


!!! tip
    **優點:方便操作使用，缺點:無法協作**     
    這種版本控制工具中最流行的是rcs，目前仍存在於許多電腦。 即使是流行的Mac OS X作業系統，都會在讀者安裝開發工具時安裝rcs命令。 此工具基本上以特殊的格式記錄修補集合（patch set，即檔案從一個版本變更到另一個版本所需資訊），並儲存於磁碟上。 它就可以藉由套用各修補集合産生各時間點的檔案內容。

## 2. 集中化版本控制系統( Centralized Version Control Systems)
---

**再一台Server上建立版本資料庫，記錄所有檔案變更的狀態，解決了協作的問題，常見的集中化版本控制系統有如：CVS、Subversion及Perforce皆具備單一伺服器，記錄所有版本的檔案，且有多個客戶端從伺服器從伺服器取出檔案。 在多年後，這已經是版本控制系統的標準。**

![](../img/2017-05-29-22-23-42.png) 

!!! tip
    **缺點是Server會掛，開發會很凌亂**  
    這樣的配置提供了很多優點，特別是相較於本地端的版本控制系統來說。 例如：每個人皆能得知其它人對此專案做了些什麼修改有一定程度的瞭解。 管理員可調整存取權限，限制各使用者能做的事。 而且維護集中式版本控制系統也比維護散落在各使用者端的資料庫來的容易。
    然而，這樣的配置也有嚴重的缺點。 最明顯的就是無法連上伺服器時。 如果伺服器當機一個小時，在這段時間中沒有人能進行協同開發的工作或者將變更的部份傳遞給其他使用者。 如果伺服器用來儲存資料庫的硬碟損毀，而且沒有相關的偏份資料。 除了使用者已取到本地端電腦的版本外，包含該專案開發的歷史的所有資訊都會遺失。 本地端版本控制系統也會有同樣的問題，只要使用者將整個專案的開發歷史都放在同一個地方，就有遺失所有資料的風險。

## 3. 分佈式版本控制系統
---
再每一台電腦都有版本資料庫，可以在Local上獨立工作，如果Server硬碟掛了也不要緊，找一份完整的換上去就復原了。這就是分散式版本控制系統(Distributed Version Control Systems, 簡稱DVCSs)被引入的原因。 在分散式版本控制系統，諸如：Git、Mercurial、Bazaar、Darcs。 客戶端不只是取出最後一版的檔案，而是完整複製整個儲存庫。 即使是整個系統賴以運作的電腦損毀，皆可將任何一個客戶端先前複製的資料還原到伺服器。
 每一次的取出動作實際上就是完整備份整個儲存庫

 ![](../img/2017-05-29-22-24-10.png) 
 
!!! tip
    **缺點是亂上加亂...，用Branch來解決吧。**  
    更進一步來說，許多這類型的系統皆能同時與數個遠端的機器同時運作。 因此讀者能同時與許多不同群組的人們協同開發同一個專案。 這允許讀者設定多種集中式系統做不到的工作流程，如：階層式模式。