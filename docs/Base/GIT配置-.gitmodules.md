 .gitmodules  
====================


> gitmodules - defining submodule properties  
[Readme](https://git-scm.com/docs/gitattributes)  

> SYNOPSIS  
$GIT_WORK_DIR/.gitmodules



Reference:

<https://blog.wu-boy.com/2011/09/introduction-to-git-submodule/>


<https://git-scm.com/book/zh-tw/v1/Git-%E5%B7%A5%E5%85%B7-%E5%AD%90%E6%A8%A1%E7%B5%84-Submodules>



<http://www.syntevo.com/doc/display/SG/Submodules>


<http://www.open-open.com/lib/view/open1328070404827.html#articleHeader1>
