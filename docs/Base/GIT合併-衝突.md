GIT合併- 衝突
====================
> 到目前為止，合併好像做的滿順利的。但如果兩個分支都改了同一個檔案，就會造成衝突(conflict)，很多人因為怕衝突，而不敢開分支，其實，在發生檔案衝突時，只要選擇你要保留哪一個版本的檔案內容，再來合併就解決啦！
所以衝突(conflict)是要人為來解決的，GIT沒有辦法幫你決定要哪個版本啊!!!

## Use Git command
-------

    mkdir merge3
    cd merge3
    git init
    echo 1 > file1.txt
    echo 1 > file2.txt
    git add file1.txt
    git commit -m "C1"
    git add file2.txt
    git commit -m "C2"

------------------------------
    git checkout -b release
    echo 大家好 > file1.txt
    echo 1 > file3.txt
    git add  --all
    git commit -m "C3"
    echo 1 > file4.txt
    git add .
    git commit -m "C4"
------------------------------
    git checkout master
    echo 我愛你 > file1.txt
    echo 1 > file5.txt
    git add  --all
    git commit -m "C5"
    echo 1 > file6.txt
    git add .
    git commit -m "C6"

我們看一下分支圖:  

![](../img/2017-06-11-18-41-57.png)


這兩個分支都改了file1.txt，release分支改成大家好，而master分支改成我愛你，接著我們將release合併到master裡，看會花生省魔術：

    git checkout master
    git merge release  


慘了！慘了！發生衝突(Conflict)，怎麼辦~怎麼辦~  

![](../img/2017-06-11-18-45-06.png)

我們先來看一下檔案狀態:  

    git status

![](../img/2017-06-11-18-46-13.png)  

>這裡多了一個檔案狀態，稱做未合併(Unmerged paths，簡單的說就是發生衝突(Conflict)啦！而且GIT會把file1.txt這個衝突檔案放在工作目錄(WD)，讓你自己決定怎麼修改。

### 其他工具衝突提示說明 :
--------

#### Smartgit  
![](../img/2017-06-11-18-51-13.png)


#### Tortoisegit  
![](../img/2017-06-11-18-47-47.png)


### 衝突解決方法:
--------

#### [修改檔案 & Git command]  

開file1.txt Git 會幫你修改檔案內容，如下:  

![](../img/2017-06-11-18-54-16.png)

> 您必需自己判斷要哪一個版本，依此範例release 分支的資料是"大家好"，而master 是"我愛你"，我將資料改成"大家好"後進行以下操作:  

    git add file1.txt
    git commit -m "合併release"
    git merge release  
    

歷史記錄如下圖所示。因為在這次的合併產生了衝突，必須修改衝突的部分，所以會建立新的提交記錄表示修改的合併提交。這樣，master的HEAD就移動到這裡了。  

![](../img/2017-06-12-00-20-45.png)  

我們再看一下分支圖變化:  

#### **未執行合併**  

![](../img/2017-06-11-18-41-57.png)

#### **衝突**    

![](../img/2017-06-18-15-43-24.png)

#### **解決衝突後合併**   

![](../img/2017-06-11-21-41-44.png)


## Use Smartgit
-------

![](../img/Git-Conflict.gif)  
