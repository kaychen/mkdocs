GIT合併 - 分支合併
====================

>合久必分、分久必合。GIT在做分支控管時也是分分合合，接下來就來講解怎麼做分支合併(merge)。

## Use Git command
-------


    mkdir merge1
    cd merge1
    git init
    echo 1 > file1.txt
    echo 1 > file2.txt
    git add file1.txt
    git commit -m "C1"
    git add file2.txt
    git commit -m "C2"

------------------------------
    git checkout -b bug
    echo 1 > file3.txt
    git add .
    git commit -m "C3"
    echo 1 > file4.txt
    git add .
    git commit -m "C4"
------------------------------
    git checkout master
    echo 1 > file5.txt
    git add .
    git commit -m "C5"
    echo 1 > file6.txt
    git add .
    git commit -m "C6"

先看一下分支圖:  
![](../img/2017-06-04-22-32-49.png)  
master分支有以下檔案:  

    file1.txt、file2.txt、file5.txt、file6.txt
bug分支有以下檔案:  

    file1.txt、file2.txt、file3.txt、file4.txt

接著要將bug合併到master分支上，首先將分支切換到master：  

    git checkout master

接著將bug分支合併進來：  

    git merge bug

> 做完合併動作後，也代表bug分支的階段性任務完成，通常會將此分支刪除，執行 command :  ```git branch -d bug```  
>因為bug分支已經合併過，用-d就能刪除，而不用-D
若不刪除分支，繼續留著也可以，就看你的決定了。


這樣就完成合併的動作了，來看一下分支圖：  

![](../img/2017-06-04-22-36-43.png)


這次的合併有幾個重點要看：

    * 首先合併後GIT會自動多了一個提交(commmit)的版本，並自動加上說明Merge branch bug。  
    * 雖然合併了，但只有master往前推進一個版本，而bug分支還是繼續停留在原本的C4提交(commit)版本上。  
    * 在master分支查看資料夾檔案，一共有6個檔案：file1.txt～file6.txt  
    * 在bug分支查看資料夾檔案，一共有4個檔案：file1.txt～file4.txt  

要解釋為何兩個分支的檔案有如此變化，我們把它畫成水流圖:  

![](../img/2017-06-04-22-39-35.png)

## Use Smartgit
-------
![](../img/Git-merge.gif)

