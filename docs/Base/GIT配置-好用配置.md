換行問題配置
====================

## 需修改Local端的 .gitconfig
------------

    [core]
        quotepath = false  # 中文檔名如實顯示而不轉碼
        autocrlf = false  # commit 及 checkout 時不根據作業系統轉換檔案的換行字元 (避免不小心改動原 repo 的換行字元)
        safecrlf = false  # 檢查文字類檔案是否混合了 CRLF 及 LF 換行字元 (搭配 autocrlf，這裡一起關閉)
        ignorecase = false  # 檔名大小寫不同時視為相異 (更動大小寫才能 commit)
        whitespace = cr-at-eol  # diff 時行尾 CRLF 不顯示 ^M
        fileMode = false  # 忽略檔案的 x 屬性 (for Windows)
        symlinks = false  # 忽略符號連結 (for Windows)
        editor = /usr/bin/vim  # 預設的文字編輯器 (for Linux)
    [alias]
        br = branch  # 依個人習慣可考慮加上 -vv
        ci = commit
        cia = commit --amend -C HEAD  # 修改目前版本，提交訊息、時間、作者等皆沿用
        co = checkout
        ch = diff --name-status -M100%  # 與 diff 類似但只列出更動的檔案，100% 相同才視為更名
        di = diff --word-diff=color --word-diff-regex='[0-9A-Za-z]+|.'  # 與 diff 類似但可比對行內差異，且支援中文
        st = status
        re = remote
        g = grep -n --color -E  # 尋找工作目錄檔案內文
        l = log --pretty=oneline --abbrev-commit --graph --decorate  # 簡短 log
        lg = log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%ai)%Creset %C(bold blue)<%an>%Creset %n'  # 一般樹狀 log
        lga = log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%ai)%Creset %C(bold blue)<%an>%Creset %n' --branches=*  # log 所有分支（類似 git lg --all 但不列出 remote 及特殊 refs）
        lgf = log --graph --pretty=format:'%Cred%H%Creset -%C(yellow)%d%Creset %s %Cgreen(%ai)%Creset %C(bold blue)<%an>%Creset %n'  # 同 lg 但輸出完整 hash 供特定需求使用
        ls = ls-tree --name-only  # git ls [-r] <commit>: 列出 <commit> 含有的目錄及檔案
        purge = !git reset --hard && git clean -fd  # 清除一切修改及未版控檔案。可加 -x 刪除 .gitignore 忽略的檔案
        rename = !mkdir -p $(dirname "$2") && git mv "$1" "$2" && true  # 更名並自動建立空目錄
        ### 進階
        rc = rebase --continue
        rs = rebase --skip
        ra = rebase --abort
        dupes = !"git ls-tree -r HEAD | cut -c 13- | sort | uniq -D -w 40"  # 列出重覆的檔案
        arc = !git update-ref "refs/archive/$1" "$1" && git branch -D "$1" && true  # 把分支移到 refs/archive/ 封存
    [gui]
        encoding = utf-8
    [i18n]
        commitEncoding = utf-8
        logOutputEncoding = utf-8
    [color]
        branch = auto
        diff = auto
        log = auto
        status = auto
        interactive = true

ref:

<https://gist.github.com/danny0838/11acfa65824940d426cd3f4403ca4ed4>
