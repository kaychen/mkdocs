Git apply
====================

## Use Git command
-------

> git-apply - Apply a patch to files and/or to the index 
>[Readme](https://git-scm.com/docs/git-apply)

    git-apply xxx.diff
    git-apply xxx.patch

### whitespace Error
-------

![](../img/Git_apply_error.png)

!!! tip  

    --whitespace=<action>
    When applying a patch, detect a new or modified line that has whitespace errors. What are considered whitespace errors is controlled by core.whitespace configuration. By default, trailing whitespaces (including lines that solely consist of whitespaces) and a space character that is immediately followed by a tab character inside the initial indent of the line are considered whitespace errors.

    By default, the command outputs warning messages but applies the patch. When git-apply is used for statistics and not applying a patch, it defaults to nowarn.



### 解決方法:
------

repos 設定:

    git config apply.whitespace nowarn

全域設定:

    git --global config apply.whitespace nowarn

## Use SmartGit
-------
    Tool-> Apply Patch... -> Select *.diff or *.patch

![](../img/Git_apply_smartgit.png)