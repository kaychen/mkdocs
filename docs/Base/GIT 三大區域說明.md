GIT 結構及狀態說明
====================  
## Git的三種區域圖
------
![](../img/2017-05-29-23-10-23.png)

*  `Working Directory (WD)` : 目前工作目錄
*  `Staging Area` : 暫存準備遞交區，也稱 git index 索引 ( Git 所有操作皆會修改 .git\index 這個二進位檔，去紀錄git 的狀態)
*  `Repository` : Git 儲存庫 (Repository)


> (1)  使用者從Git目錄下載某一特定版本檔案到工作區。  
> (2)	使用者修改工作目錄內的檔案。   
> (3)	使用者將檔案的快照新增到暫存區域(變更index)。   
> (4)	使用者做提交的動作，將存在暫存區域的檔案快照永久的儲存在Git目錄。    


Staging Area 算是 Git 獨有的功能，有什麼好處呢?  

* Working tree 可能很亂，包含了想要 commit 的內容和不相關的實驗修改等
* Staging area 的設計可以讓你只 commit 想要的檔案，甚至是想要的部份修改而已
* Staging area  也稱 git index 索引，目的主要用來紀錄「有哪些檔案即將要被提交到下一個 commit 版本中」。

	

## 檔案在各個區域間的切換演示:
------

![](../img/2017-05-30-22-14-33.png)

## WD ↔ Stage  
------

[Git command line] 

	git add  (WD -> Stage)
	git reset <filename> (Stage -> WD)
		
[SmarGit]	

	Stage / UnStage  
![](../img/Git-Add.gif)

[TortoiseGit]

	(WD -> Stage)  右鍵點選Add...  
	(Stage -> WD)  右鍵點選Revert...
		
## Stage ↔  Git Repository  
------

![](../img/2017-05-30-22-22-18.png)

[Git command line]  
		
	git commit (Stage -> Git Repository)
	git reset -- files (Git Repository -> Stage)

## Git Repository ↔  Stage or WD  
------
[Git command line]  
	
	git commit -a 相當於執行 git add 把所有目前 WD 下的檔案加入 Stage 再執行 git commit.  
	git commit <files> 進行一次包含最後一次提交加上工作目錄中檔快照的提交。並且檔被添加到暫存區域。
	git checkout HEAD <files> 回滾到複製最後一次提交。
