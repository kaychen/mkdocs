SSL certificate problem
====================

## 錯誤訊息:
> SSL Certificate problem: unable to get local issuer

    若self-signed certificate證書無法驗證時，會發生此錯誤。 

## 解決方式:
-----

(1) 告訴git不要使用SSL驗證(建議使用此方式)：

    git config --global http.sslVerify false

(2) 告訴git認證的路徑並且重新安裝git


    git config --system http.sslCAPath /path/to/git/certificates

   [ref here](https://blogs.msdn.microsoft.com/phkelley/2014/01/20/adding-a-corporate-or-self-signed-certificate-authority-to-git-exes-store/)


Reference:

<https://confluence.atlassian.com/bitbucketserverkb/ssl-certificate-problem-unable-to-get-local-issuer-certificate-816521128.html>