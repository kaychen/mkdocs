GIT暫存-Stash
====================
> 如果你正在開發一個新的功能，並在feature功能分支進行開發，忽然老闆叫你放下手邊工作， 去解決一個bug，這時怎麼辦呢？你的Work Direction和Stage Area都還留有未處理的檔案， 這時無法切換到其他分支，除非你馬上將所有檔案提交(Commit)出一個版本，但這樣的版本 內容是無意義的，有其他方法嗎？
這時救星出現了，你可以用Stash指令將你目前的Work Direction和Stage Area的檔案先 暫存起來，等到bug解決完後，再回來將暫存取出，就可以再繼續開發啦！

## Use Git command
-------

    mkdir stash1
    cd stash1
    git init
    echo 1 > README.md
    git add .
    git commit -m "init"

------------------------------  

    git branch feature
    echo 1 > 1.txt
    git add .
    git commit -m "C1"
    git branch bug
------------------------------

    git checkout feature
    echo 1 > 2.txt
    git add .
    git commit -m "C2"
------------------------------

    echo 1 > a.txt
    echo 1 > b.txt
    echo 1 > c.txt
    git add c.txt
------------------------------

我們看一下分支圖:  

![](../img/2017-06-18-16-25-56.png)

我們看一下狀態: 

![](../img/2017-06-18-16-27-29.png)

> 目前所在分支是feature，而且WD和Stage有開發未完成的檔案：  
    • WD區: a.txt、b.txt  
    • Stage區: c.txt


接著我要將這些檔案都先暫存起來，可用以下指令  

    git satsh  

git satsh只會將Stage區的c.txt暫存起來而已。 如果要將WD區和Stage區的檔案a.txt, b.txt, c.txt都暫存起來，就要執行：

    git stash -u

> 如果怕自己忘記這個暫存放了啥，可加上註解，指令是```git stash save -u "註解" ```  
>SmartGit 分支圖會有 stash 的相關圖示可以參考  
 ![](../img/2017-06-18-18-05-37.png)  


下完指令後，這3個檔案都暫存起來了，看起來乾乾淨淨^__^  

![](../img/2017-06-18-16-36-19.png)

此時我們便可以切換至bug分支，開始修正錯誤：    

    git branch bug
    echo fix > 1.txt
    git add .
    git commit -m "fix bug 1.txt"

修正完bug後，回到feature分支：  

    git checkout feature

我們先來查詢看暫存是否還在，指令如下：  

    git stash list

![](../img/2017-06-18-16-40-15.png)

果然可以看到剛才那筆暫存，接下來要把暫存再叫回來，取回暫存的指令是：  

    git stash pop

![](../img/2017-06-18-16-41-46.png)  

YA~檔案又回來了，又可以繼續未完成的工作囉~~  

>補充：如果要刪除暫存，指令是git stash clear


## Use Smartgit
-------


## ```將檔案放置 stash 區域```  

![](../img/Git-Stash1.gif)  

## ```切換至bug分支，開始修正錯誤```  

![](../img/Git-Stash2.gif)  

## ```切換至feature，取回之前存的stash File```  

![](../img/Git-Stash3.gif)  
