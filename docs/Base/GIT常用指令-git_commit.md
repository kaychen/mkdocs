GIT 常用指令 (git commit)
====================  

## Use Git command  

> git-commit - Record changes to the repository  
>[Readme](https://git-scm.com/docs/git-commit)

!!! tip ""  
    執行git commit  就是把檔案從Staging 移至 Repository

## Use SmartGit
-------

Push This button :  
 
![](../img/2017-06-03-18-28-36.png)


## 修改上一次的commit
-------

### Use Git command  

若手誤打太快， commit 訊息打錯時，我們可以使用 ``` git commit --amend ``` 來幫助我們重新修改：

    git commit --amend "re-commnit"

### Use SmartGit

## 定義預設 Git commit message template
    
    vim ~/.gitmessage.txt
    git config --global commit.template ~/.gitmessage.txt

!!! note 
    '#' 開頭在 git commit視為註解 

## 使用Commit 關閉 gitlab /github issue

Gitlab:

<https://docs.gitlab.com/ee/user/project/issues/automatic_issue_closing.html#default-closing-pattern-value>

Github:

<https://help.github.com/articles/closing-issues-using-keywords/'>