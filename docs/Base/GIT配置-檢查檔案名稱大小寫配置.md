檢查檔案名稱大小寫配置
====================

>在 Git 的 Windows 和 MacOS 的 File system 底下會將 “名稱相同” 但大小寫不同的檔案辨認為同一個檔案，如果你需要修改大小寫檔名的時候就會無法 update，只有在 Linux 才會區別大小寫不同為不一樣的檔案。
    
查看你的設定:  
    
    git config -l | grep ignorecase

### 解決方式:

    git config --global core.ignorecase false

!!! tip  

    建議都使用 core.ignorecase=false 去區分大小寫

[參考資料](https://shazi.info/git-%E5%A6%82%E4%BD%95%E5%88%A4%E5%88%A5%E6%AA%94%E6%A1%88%E5%90%8D%E7%A8%B1%E5%A4%A7%E5%B0%8F%E5%AF%AB%E4%B8%8D%E5%90%8C-ignorecase/)
