GIT合併 - 回復合併
====================
> 如果合併後，發現做錯了，想要反悔怎麼辦呢？

## Use Git command
-------

    git checkout master
    git log --oneline
    git reset --hard [C6的commit ID]

![](../img/2017-06-04-23-17-18.png)

看吧！瑞凡，我們回去了~~  

![](../img/2017-06-04-23-18-17.png)

有更簡單的方法，可以利用ORIG_HEAD別名直接回到上一版本

    git reset --hard ORIG_HEAD

這裡reset指令中的--hard表示強制將工作目錄(WD)的檔案回復到前一版本。  
若你想返回前一版本，又要保留目前工作目錄(WD)的檔案，可用--soft，通常比較少用。


!!! note 

    git reset (這裡接某個你想回到的commit的代碼)  
    git reset 放棄 add，但保留修改  
    git reset --hard 放棄所有修改，回到上個 commit 完成後的狀態  
    git reset --hard HEAD 回到最新一個 commit 版本  
    git reset --hard HEAD^ 回到前一個 commit 版本  
    git reset --hard HEAD^^ 回到前前一個 commit 版本  
    git reset --hard HEAD~2 回到前前前一個 commit 版本  
    git reset --soft HEAD^ 回復到 commit 提交前的狀態( HEAD^ 代表前 1 次)  
    git rebase -i 編輯commit 可以調換順序或刪除  
    [Detail Reference](https://dotblogs.com.tw/wasichris/2016/04/29/225157)



## Use Smartgit
-------
![](../img/Git-merge_revert.gif)
