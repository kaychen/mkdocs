GIT Hash 操作
====================

## 查詢Hash ID
-------
### All Tag
    git show-ref --tags
### Show tag/branch path & ID
    git show-ref <tag/branch>
### Only show tag/branch hash ID
    git show-ref --hash <tag/branch>


## Hash ID 反查對應的物件
-------
### 查看物件內容 (pretty print)
    git cat-file -p  <HashID>
### 查看物件類型 (type)
    git cat-file -t <HashID>
### 查看物件大小 (size)
    git cat-file -s <HashID>
### 計算物件編號 (object id)
    git hash-object <FileName>

Ref:

<https://git-scm.com/docs/git-show-ref>
<https://git-scm.com/docs/git-cat-file>
<https://git-scm.com/docs/git-hash-object>