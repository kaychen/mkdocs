Git rerere配置
====================

## git conflict 自動記錄(Git rerere)

>  rerere = reuse recorded resolution   
它會讓 git 記住所有 conflict 是如何解決的，然後當同樣的 conflict 產生時重用它們，而且它還會讓你確定這一操作是否是正確的


### 開啟方式:
    
    git config --global rerere.enabled true

[參考資料](https://ruby-china.org/topics/15809) 

