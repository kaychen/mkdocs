GIT 常用指令 (git init)
====================  

## Use Git command
-------
> git-init - Create an empty Git repository or reinitialize an existing one  
>[Readme](https://git-scm.com/docs/git-init)

	mkdir working_dir && cd working_dir
	git init
	cd .git
	tree –F

!!! note ""  
	執行git-init的時候，會在working directory下建立.git目錄(預設為隱藏檔)，裡面放的是git repository的檔案，有了這些檔案，你才可以在這個working directory裡做git的操作。

Output:
```
|--　HEAD
|--　branches/
|--　config
|--　description
|--　hooks/
|　　　|--　applypatch-msg.sample*
|　　　|--　commit-msg.sample*
|　　　|--　post-commit.sample*
|　　　|--　post-receive.sample*
|　　　|--　post-update.sample*
|　　　|--　pre-applypatch.sample*
|　　　|--　pre-commit.sample*
|　　　|--　pre-rebase.sample*
|　　　|--　prepare-commit-msg.sample*
|　　　`--　update.sample*
|--　info/
|　　　`--　exclude
|--　objects/
|　　　|--　info/
|　　　`--　pack/
`--　refs/
 　　　|--　heads/
 　　　`--　tags/
```


名稱       | 描述
--------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
HEAD          | 指向目前的branch(master), 但可看到refs/heads下面目前沒有master，因為才剛建好，目前是空的，一直要到你執行了第一個commit，才會出現檔案
config        | 目前這個git repository的config。
description   | 記載對這個git repository的敘述，某些tool會顯示這邊的敘述，像是gitweb。
hooks/        | 似乎指的是小外掛，git在執行某些動作前後會執行這些hooks，只是default是關，要把.smaple拿掉才能enable (也要有x權限)
              | 最近有用到這裡面的東西了，hooks裡面的scripts可以定義在push前或push後幫你自動執行一些動作，做什麼動作就完全看相對應的script怎麼寫。例如，post-receive會去call post-receive-email，在push commit完後會自己發email出來，現在我們是將這個email改寫成updatelist，通知所有的人這是第幾個updatelist以及有哪些檔案被更改了，還挺好用的咧~
info/exclude  | 指出project中哪些檔案是不需列入git中管理的，但這裡的設定不會跟著git clone分享給別人，所以需要分享給所有人的設定，請用.gitignore檔案設定。
objects/      | 每個commit在這目錄下都會有個相對應的檔案來記錄commit的所有動作。
objects/pack/ | commit 的東西在pack之前會是一支支檔案散布在objects/下，pack起來之後，會統一打包到這目錄下。會有個.idx跟.pack檔，pack檔會非常大。
refs/heads/   | 個branch都會在這目錄下對應到一個檔案，記載著那個branch的head是指到哪個cid
refs/tags/    | 也是記載著tag是標註在哪個cid上。refs/下的東西，在pack之後都會把所有內容直接集中到packed-refs這隻檔案裡，若ref後來又有更新，則又會在相對應的目錄裡產生檔案，最後ref的結果是以目錄裡的檔案為準。

!!! note ""    
	參數說明
	**--template=<template_directory>**  
	這個參數的意思是，到template_directory取得.git的template，default目錄在/usr/share/git-core/templates，看一下就知道意思了。
	裡面有description、hooks/、exclude，你可以自訂一個新的default template directory。
	ex. 你可以修改template來更改git init出來後，exclude所預先設定的檔名。  

	**--bare**  
	在不加--bare所init完的結果，會是在目前的working目錄下建立.git目錄，並把上述的git相關檔案放在.git目錄裡。
	加了--bare的話，則是不建.git目錄，而把裡面的檔案直接放在目前目錄下。  
	適合在沒有修改檔案或開發的機器上，ex GIT server。
	好處是只要maintain git repository，project的資料只存在repository中，不需要再複製一份最新的版本出來，會省點空間。   

	**--shared=xxx**  
	這個會決定.git目錄的權限，只要某使用者沒有這目錄的權限，自然就無法對這個git repository做操作。
	這個權限設定跟working directory裡面的project file的權限是分開的。有可能使用者看得到working directory裡的檔案，也可以修改，可是卻沒有辦法push到這個repository裡。
	因為git的操作都會更改到.git目錄裡面的東西，沒有這個目錄的權限就代表不能做相關的操作。 
	若不加這參數則參考系統的umask設定 (umask0022，init出來的權限就是0644)  

	**--shared=umask  --shared=false**   
	==> 這兩個，加了等於沒加，就是去參考umask，其實就是default
	**--shared=group --shared=true --shared=all  --shared=everybody **  
	==> 好像沒有差別，結果都會變成是0664  
	**--shared=0600**  
	==> 只有使用者能讀寫  
	**--shared=0640**  
	==> 使用者跟同group  
	這個參數會影響到.git/config裡面的sharedrepository設定

## Use SmartGit - ( Create New Repository )
-------

	左上角選 Repository -> Add or Create 

![](../img/2017-05-30-19-27-03.png)

	選擇要加入的 Repository 或新增 Repository  
![](../img/2017-05-30-19-25-57.png)

	空目錄會有以下訊息,按下 Initialize 繼續  
![](../img/2017-05-30-19-26-09.png)  

## Use TortoiseGit - ( Create New Repository )
-------

	空目錄下點選右鍵->Git Create Repository here  

![](../img/2017-05-30-19-18-02.png)

	預設不選 Make Bare  
![](../img/2017-05-30-19-15-54.png)

	建立後會產生.git 的隱藏檔  
![](../img/2017-05-30-19-21-18.png)

