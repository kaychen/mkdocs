GIT Export
====================

> git-archive - Create an archive of files from a named tree
>[Readme](https://git-scm.com/docs/git-archive)


## Tortoisegit
-------
Diff ->Select your file -> Export select to...

![](../img/tortoisegit_export.png)


## Smartgit
-------
Add git archive command 


### Archive all 
Edit -> Preferences -> Tools -> Add .. -> Edit parameter :   
Menu item Name: `Archive`  
Commnad: `${git}`  
Arguments: `archive --format=zip --output=${fileSave} ${commit}`  
Handles: `Refs`  
Show output: `check`  

![](../img/Git_archive_1.png)

### Archive diff file

Step 1. add global .gitconfig

    [alias]
    DiffExport = "!f() { \
        git archive -o $1_Diff_$(date +"%Y%m%d").zip $1 $(git diff --name-only $1^..$1) ; \
    }; f"

Step 2. Smartgit Setting

Edit -> Preferences -> Tools -> Add .. -> Edit parameter :   
Menu item Name: `ArchiveDiff`  
Commnad: `${git}`  
Arguments: `DiffExport ${commit}`  
Handles: `SHAs`  
Show output: `check`  

![](../img/Git_archive_2.png)


Ref:  

<https://blog.miniasp.com/post/2014/04/01/Git-Export-Only-Added-Modified-Files.aspx>


<https://stackoverflow.com/questions/13181249/export-modified-files-from-multiple-commits-in-git>
