GIT 分支 - (刪除及回復)
====================

>刪除分支的指令為 ```branch -d```，例如要刪除feature分支，__**記得要跳出此分支**__，總不可能自己刪自己吧!!

## GIT 刪除分支
-------
### Use Git command
-------
    git checkout master
    git branch -d feature

結果刪不掉，怎麼回事？來看一下錯誤訊息：  

![](../img/2017-06-04-21-28-25.png)

>原來GIT怕你誤刪，如果此分支尚未和別的人支合併，是無法直接刪除的，必須透過 ```branch -D``` 來強制刪除：

    git branch -D feature

![](../img/2017-06-04-21-30-52.png)

### Use Smartgit
-------
 ![](../img/Git-delete.gif)

!!! tip ""
    請注意SmartGit 執行刪除預設使用 ```branch -D``` 

## GIT 回復分支
-------
### Use Git command
-------
那再問一個問題，C3、C4這兩個commit還在嗎？ 看一下分支圖:  

![](../img/2017-06-04-21-54-50.png)

>C3、C4不見了，疑？刪除分支並不會刪掉提交(commit)啊！怎會這樣呢？ 其實C3、C4還在，只是「隱藏」起來罷了，我們來施點魔法找出來吧！

    git reflog
    git checkout [C4的SHA1值]
    git checkout -b feature

![](../img/2017-06-04-22-02-35.png)

再去看分支圖，是不是又長出來了啊！  
![](../img/2017-06-04-21-10-18.png)

### Use Smartgit
-------
![](../img/Git-revert.gif)

