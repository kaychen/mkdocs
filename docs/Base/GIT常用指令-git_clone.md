GIT 常用指令 (git clone)
====================  

## Use Git command
-------
> git-clone - Clone a repository into a new directory  
>[Readme](https://git-scm.com/docs/git-clone)

!!! note ""  
	有時候 Clone 整個專案下來檔案會很大，可以用 --depth 選擇要clone 的版本數  

* 獲取目前最新版及前一個版本:  
	
		git clone git@xxx.xxx.xxx --depth=1  

* 可以rev-list 查看當前branch 的SHA list. 
* 可以rev-list 查看當前branch 的SHA list. 

		eg. git rev-list master 
	
## Use SmartGit
-------
![](../img/2017-05-31-21-38-41.png)

## Use TortoiseGit
-------
 ![](../img/2017-05-31-21-36-38.png)

## Other (sparse-checkout)
-------
### 操作說明:  
-------
另外還可利用修改 sparse-checkout 參數定義要Clone的資料: 

建立一個空的git專案:

	git init new.cdnjs && cd new.cdnjs   

在專案裡面啟用sparse-checkout

	git config core.sparseCheckout true

設定你要checkout哪些檔案(直接寫到.git/info/sparse-checkout，多個規則可寫多行)，例如我只要ajax/libs/jquery/底下的所有檔案

	echo '/ajax/libs/jquery/*' >> .git/info/sparse-checkout

設定remote(要從哪裡clone/pull?)

	git remote add origin git://github.com/cdnjs/cdnjs.git

然後就可以開始pull了(這邊可以加上前面說的shallow pull，加上–depth=n)

	git pull origin master

[Reference](https://www.peterdavehello.org/2015/05/use-git-sparse-checkout-and-shallow-clone-pull-to-increase-work-efficient/)
