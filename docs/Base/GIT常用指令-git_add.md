GIT 常用指令 (git add)
====================  

## Use Git command
-------
> git-add - Add file contents to the index  
>[Readme](https://git-scm.com/docs/git-add)

!!! tip ""  
    執行git add  就是把檔案移至Staging area

## Use SmartGit
-------
Push This button :  

![](../img/2017-06-03-18-14-15.png)  

## Use TortoiseGit
-------
    於WD 中選擇檔案右鍵點選Add...

